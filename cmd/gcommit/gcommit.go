package main

import (
	"log"
	"os"
	
	"gopkg.in/src-d/go-git.v4"
	//"gopkg.in/src-d/go-git.v4/plumbing"
)

func main() {
	log.Println("Starting gcommit.")
	
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Git directory: ", dir)
	
	commitMessage := os.Args[1]
	log.Println("Commit message: ", commitMessage)
	
	// Opens the current existing repo
	repo, err := git.PlainOpen(dir)
	if err != nil {
		log.Fatal(err)
	}
	
	headRef, err := repo.Head()
	if err != nil {
		log.Fatal(err)
	}
	
	branchName := headRef.Name().Short()
	
	log.Println("Head reference: ", headRef)
	log.Println("Branch name: ", branchName)

	fullMessage := branchName + " - " + commitMessage
	log.Println("Full commit message: ", fullMessage)
	
	worktree, err := repo.Worktree()
	if err != nil {
		log.Fatal(err)
	}
	
	err = worktree.AddGlob("*")
	if err != nil {
		log.Fatal(err)
	}
	
	log.Println("Finished gcommit.")
}