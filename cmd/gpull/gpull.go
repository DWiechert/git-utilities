package main

import (
	"log"
	"os"
	
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
)

func main() {
	log.Println("Starting gpull.")
	
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Git directory: ", dir)
	
	branchToDelete := os.Args[1]
	log.Println("Branch to delete: ", branchToDelete)
	
	// Opens the current existing repo
	repo, err := git.PlainOpen(dir)
	if err != nil {
		log.Fatal(err)
	}
	
	worktree, err := repo.Worktree()
	if err != nil {
		log.Fatal(err)
	}
	
	log.Println("Worktree: ", worktree)
	err = worktree.Checkout(&git.CheckoutOptions{
		Branch: plumbing.NewBranchReferenceName("master"),
	})
	if err != nil {
		log.Fatal(err)
	}
	
	log.Println("Finished gpull.")
}